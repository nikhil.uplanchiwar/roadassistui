import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { User } from './models/user'  

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = 'http://localhost:61844/Api/UserProfile';  
  constructor(private http: HttpClient) { }  
  getAllUser(): Observable<User[]> {  
    return this.http.get<User[]>(this.url + '/GetAllUser'); 

}
GetUserById(Id: number): Observable<User> {  
  return this.http.get<User>(this.url + '/GetUserById/' + Id);  
}  
createUser(users: User): Observable<User> {  
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
  return this.http.post<User>(this.url + '/AddUser/',  
  users, httpOptions);  
}  
updateUser(users: User): Observable<User> {  
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
  return this.http.put<User>(this.url + '/UpdateUser/',  
  users, httpOptions);  
}  
deleteUserById(Id: number): Observable<number> {  
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
  return this.http.delete<number>(this.url + '/DeleteUser?id=' +Id,  
httpOptions);  
}  

}
