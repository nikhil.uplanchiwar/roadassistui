import { Component, OnInit } from '@angular/core';  
import { FormBuilder, Validators } from '@angular/forms';  
import { Observable } from 'rxjs';  
import { Booking } from 'src/app/models/booking';
import { BookingServiceService } from 'src/app/shared/booking-service.service';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  dataSaved = false;  
  bookingForm: any;  
  AllUsers: Observable<Booking[]>;  
  BookingIdUpdate = null;  
  massage = null;  
  constructor(private formbulider: FormBuilder, private BookingService:BookingServiceService) { }  

  ngOnInit(): void {
    this.bookingForm = this.formbulider.group({  
      Charges: ['', [Validators.required]],  
      Location: ['', [Validators.required]],  
      State: ['', [Validators.required]],  
      City: ['', [Validators.required]],  
      TotalAmountPayment: ['', [Validators.required]],  
      StartOTP: ['', []], 
      EndOTP: ['', []],   
    });  
    this.loadAllBooking();  
  }

  loadAllBooking() {  
    this.AllUsers = this.BookingService.GetAllBooking();  
  }  
  onFormSubmit() {  
    this.dataSaved = false;  
    const book = this.bookingForm.value;  
    this.BookingService.createBooking(book);  
    this.bookingForm.reset();  
  }  
  loadUserToEdit(Id: number) {  
    this.BookingService.GetBookingById(Id).subscribe(book=> {  
      this.massage = null;  
      this.dataSaved = false;  
      this.BookingIdUpdate = book.ID;  
      this.bookingForm.controls['Charges'].setValue(book.Charges);  
     this.bookingForm.controls['Location'].setValue(book.Location);  
      this.bookingForm.controls['TotalAmount'].setValue(book.TotalAmount); 
      this.bookingForm.controls['State'].setValue(book.State);  
      this.bookingForm.controls['City'].setValue(book.City);  
      this.bookingForm.controls['StartOTP'].setValue(book.StartOTP);  
      this.bookingForm.controls['EndOTP'].setValue(book.EndOTP);  
      this.bookingForm.controls['PaymentStatus'].setValue(book.PaymentStatus);
    });  
  
  }  
  CreateUser(employee: Booking) {  
    if (this.BookingIdUpdate == null) {  
      this.BookingService.createBooking(employee).subscribe(  
        () => {  
          this.dataSaved = true;  
          this.massage = 'Record saved Successfully';  
          this.loadAllBooking();  
          this.BookingIdUpdate = null;  
          this.bookingForm.reset();  
        }  
      );  
    } else {  
      employee.ID = this.BookingIdUpdate;  
      this.BookingService.UpdateBooking(employee).subscribe(() => {  
        this.dataSaved = true;  
        this.massage = 'Record Updated Successfully';  
        this.loadAllBooking();  
        this.BookingIdUpdate = null;  
        this.bookingForm.reset();  
      });  
    }  
  }   
  deleteEmployee(Id: number) {  
    if (confirm("Are you sure you want to delete this ?")) {   
    this.BookingService.DeleteBooking(Id).subscribe(() => {  
      this.dataSaved = true;  
      this.massage = 'Record Deleted Succefully';  
      this.loadAllBooking();  
      this.BookingIdUpdate = null;  
      this.bookingForm.reset();  
  
    });  
  }  
}  
  resetForm() {  
    this.bookingForm.reset();  
    this.massage = null;  
    this.dataSaved = false;  
  }  
}  
