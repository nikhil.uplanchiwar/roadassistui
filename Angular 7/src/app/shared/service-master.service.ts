import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { ServiceMaster } from '../models/service-master' 

@Injectable({
  providedIn: 'root'
})
export class ServiceMasterService {
  url = 'http://localhost:61844/Api/Service';  
  constructor(private http: HttpClient) { }  
  getAllService(): Observable<ServiceMaster[]> {  
    return this.http.get<ServiceMaster[]>(this.url + '/GetAllServices'); 
  }
  GetServiceById(Id: number): Observable<ServiceMaster> {  
    return this.http.get<ServiceMaster>(this.url + '/GetServiceById/' + Id);  
  }  
  createService(service: ServiceMaster): Observable<ServiceMaster> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<ServiceMaster>(this.url + '/AddService/',  
    service, httpOptions);  
  }  
  updateService(service: ServiceMaster): Observable<ServiceMaster> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.put<ServiceMaster>(this.url + '/UpdateServiceById/',  
    service, httpOptions);  
  }  
  deleteServiceById(Id: number): Observable<number> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.delete<number>(this.url + '/DeleteService?id=' +Id,  
  httpOptions);  
  }  
  }