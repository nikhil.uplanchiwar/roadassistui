import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';  
import { FormBuilder, Validators } from '@angular/forms'; 
import { ServiceMaster } from 'src/app/models/service-master';
import { ServiceMasterService } from 'src/app/shared/service-master.service';

@Component({
  selector: 'app-service-master',
  templateUrl: './service-master.component.html',
  styleUrls: ['./service-master.component.css']
})
export class ServiceMasterComponent implements OnInit {
  dataSaved = false;  
  serviceForm: any;  
  AllService: Observable<ServiceMaster[]>;  
  serviceIdUpdate = null;  
  massage = null;  
  constructor(private formbulider: FormBuilder, private serviceMaster:ServiceMasterService) { }  

  ngOnInit() {
    this.serviceForm = this.formbulider.group({  
      ServiceName: ['', [Validators.required]],  
      Description: ['', [Validators.required]],  
        
    }); 
    this.loadAllService(); 
  }
   

  
  loadAllService() {  
    this.AllService = this.serviceMaster.getAllService();  
  }  
  

  onFormSubmit() {  
    this.dataSaved = false;  
    const user = this.serviceForm.value;  
    this.serviceMaster.createService(user);  
    this.serviceForm.reset();  
  }  
  loadUserToEdit(ID: number) {  
    this.serviceMaster.GetServiceById(ID).subscribe(user=> {  
      this.massage = null;  
      this.dataSaved = false;  
      this.serviceIdUpdate = user.ID;  
      this.serviceForm.controls['ServiceName'].setValue(user.ServiceName);  
     this.serviceForm.controls['Description'].setValue(user.Description);  
       
    });  
  
  }  
  CreateUser(employee: ServiceMaster) {  
    if (this.serviceIdUpdate == null) {  
      this.serviceMaster.createService(employee).subscribe(  
        () => {  
          this.dataSaved = true;  
          this.massage = 'Record saved Successfully';  
          this.loadAllService();  
          this.serviceIdUpdate = null;  
          this.serviceForm.reset();  
        }  
      );  
    } else {  
      employee.ID = this.serviceIdUpdate;  
      this.serviceMaster.updateService(employee).subscribe(() => {  
        this.dataSaved = true;  
        this.massage = 'Record Updated Successfully';  
        this.loadAllService();  
        this.serviceIdUpdate = null;  
        this.serviceForm.reset();  
      });  
    }  
  }   
  deleteEmployee(Id: number) {  
    if (confirm("Are you sure you want to delete this ?")) {   
    this.serviceMaster.deleteServiceById(Id).subscribe(() => {  
      this.dataSaved = true;  
      this.massage = 'Record Deleted Succefully';  
      this.loadAllService();  
      this.serviceIdUpdate = null;  
      this.serviceForm.reset();  
  
    });  
  }  
}  
  resetForm() {  
    this.serviceForm.reset();  
    this.massage = null;  
    this.dataSaved = false;  
  }  
}  

