import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { Booking } from '../models/booking'  

@Injectable({
  providedIn: 'root'
})
export class BookingServiceService {

  url = 'http://localhost:61844/Api/BookingDetails';  
  constructor(private http: HttpClient) { }  
  GetAllBooking(): Observable<Booking[]> {  
    return this.http.get<Booking[]>(this.url + '/GetAllBooking'); 
}
GetBookingById(Id: number): Observable<Booking> {  
  return this.http.get<Booking>(this.url + '/GetBookingById/' + Id);  
}  
createBooking(book: Booking): Observable<Booking> {  
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
  return this.http.post<Booking>(this.url + '/AddBooking/',  
  book, httpOptions);  
}  
UpdateBooking(book: Booking): Observable<Booking> {  
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
  return this.http.put<Booking>(this.url + '/UpdateBooking/',  
  book, httpOptions);  
}  
DeleteBooking(Id: number): Observable<number> {  
  const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
  return this.http.delete<number>(this.url + '/DeleteBooking?id=' +Id,  
httpOptions);  
}  
}

