export class Booking {
    
    ID	:number;
    ServiceID	:number;
    MechanicID	:number;
    Charges	:number;
    Location:string;
    City	:string;
    State	:string;
    Status	:boolean;
    TotalAmount	:string;
    PaymentStatus:string;	
    StartOTP	:number;
    EndOTP:number;
}
