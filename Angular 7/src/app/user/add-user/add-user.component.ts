import { Component, OnInit } from '@angular/core';  
import { FormBuilder, Validators } from '@angular/forms';  
import { Observable } from 'rxjs';  
import { User } from 'src/app/models/user';
import { UserProfileService } from 'src/app/shared/user-profile.service';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  dataSaved = false;  
  userForm: any;  
  AllUsers: Observable<User[]>;  
  userIdUpdate = null;  
  massage = null;  
  constructor(private formbulider: FormBuilder, private userService:UserProfileService) { }  

  ngOnInit() {
    this.userForm = this.formbulider.group({  
      Name: ['', [Validators.required]],  
      Address: ['', [Validators.required]],  
      EmailId: ['', [Validators.required]],  
      State: ['', [Validators.required]],  
      City: ['', [Validators.required]],  
      PinCode: ['', [Validators.required]],  
    });  
    this.loadAllUsers();  

  }
  loadAllUsers() {  
    this.AllUsers = this.userService.getAllUser();  
  }  
  onFormSubmit() {  
    this.dataSaved = false;  
    const user = this.userForm.value;  
    this.userService.createUser(user);  
    this.userForm.reset();  
  }  
  loadUserToEdit(userId: number) {  
    this.userService.GetUserById(userId).subscribe(user=> {  
      this.massage = null;  
      this.dataSaved = false;  
      this.userIdUpdate = user.Id;  
      this.userForm.controls['Name'].setValue(user.Name);  
     this.userForm.controls['Address'].setValue(user.Address);  
      this.userForm.controls['Email'].setValue(user.Email); 
      this.userForm.controls['Phone1'].setValue(user.Phone1);  
      this.userForm.controls['State'].setValue(user.State);  
      this.userForm.controls['City'].setValue(user.City);  
      this.userForm.controls['PinCode'].setValue(user.Pincode);  
    });  
  
  }  
  CreateUser(employee: User) {  
    if (this.userIdUpdate == null) {  
      this.userService.createUser(employee).subscribe(  
        () => {  
          this.dataSaved = true;  
          this.massage = 'Record saved Successfully';  
          this.loadAllUsers();  
          this.userIdUpdate = null;  
          this.userForm.reset();  
        }  
      );  
    } else {  
      employee.Id = this.userIdUpdate;  
      this.userService.updateUser(employee).subscribe(() => {  
        this.dataSaved = true;  
        this.massage = 'Record Updated Successfully';  
        this.loadAllUsers();  
        this.userIdUpdate = null;  
        this.userForm.reset();  
      });  
    }  
  }   
  deleteEmployee(Id: number) {  
    if (confirm("Are you sure you want to delete this ?")) {   
    this.userService.deleteUserById(Id).subscribe(() => {  
      this.dataSaved = true;  
      this.massage = 'Record Deleted Succefully';  
      this.loadAllUsers();  
      this.userIdUpdate = null;  
      this.userForm.reset();  
  
    });  
  }  
}  
  resetForm() {  
    this.userForm.reset();  
    this.massage = null;  
    this.dataSaved = false;  
  }  
}  


